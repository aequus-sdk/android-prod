package mobi.aequus.demo.mainscreen;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import mobi.aequus.demo.R;
import mobi.aequus.sdk.publisher.*;

public class BannerProgrammaticFragment extends Fragment {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private static final String PLACEMENT_NAME = "YOUR_PLACEMENT_NAME";

    private final String TAG = getClass().getSimpleName();
    private AequusBannerAdView bannerView;

    Button startButton, showButton, hideButton;
    FrameLayout bannerPlaceholder;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = View.inflate(requireActivity(), R.layout.fragment_banner_programmatic, container);
        startButton = view.findViewById(R.id.start);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startClick();
            }
        });

        showButton = view.findViewById(R.id.show);
        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showClick();
            }
        });
        showButton.setEnabled(false);

        hideButton = view.findViewById(R.id.hide);
        hideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideClick();
            }
        });
        hideButton.setEnabled(false);

        bannerPlaceholder = view.findViewById(R.id.banner_placeholder);
        progressBar = view.findViewById(R.id.banner_progressbar);

        return view;
    }

    private void createBannerAd() {
        bannerView = Aequus.createBanner(requireActivity(), PLACEMENT_NAME);
        bannerView.setListener(createBannerListener());
    }

    private void startClick() {
        startButton.setEnabled(false);
        showButton.setEnabled(true);
        hideButton.setEnabled(true);
        progressBar.setVisibility(View.VISIBLE);

        createBannerAd();
        bannerPlaceholder.addView(
                bannerView, new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
        );
    }

    private void showClick() {
        bannerView.show();
    }

    private void hideClick() {
        bannerView.hide();
    }

    private BannerListener createBannerListener() {
        return new BannerListener() {
            @Override
            public void onAdLoadSuccess(@NotNull AequusAd aequusAd) {
                // Banner ad is was loaded. It will now be shown
                showToastAndLog("onAdLoadSuccess");
            }

            @Override
            public void onAdLoadFailed(@NotNull AequusAdError aequusAdError) {
                // Banner ad is was not loaded.
                showToastAndLog("onAdLoadFailed");
            }

            @Override
            public void onAdShowSuccess(@NotNull AequusAd aequusAd) {
                // Banner ad is was shown
                showToastAndLog("onAdShowSuccess");
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onAdShowFailed(@NotNull AequusAdError aequusAdError) {
                // Banner ad was not shown
                showToastAndLog("onAdShowFailed");
            }

            @Override
            public void onAdHidden(@NotNull AequusAd aequusAd) {
                // Banner ad was hidden
                showToastAndLog("onAdHidden");
            }

            @Override
            public void onAdClicked(@NotNull AequusAd aequusAd) {
                // Banner ad was clicked
                showToastAndLog("onAdClicked");
            }
        };
    }

    private void showToastAndLog(String msg) {
        Log.i(TAG, msg);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}