package mobi.aequus.demo.mainscreen;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import mobi.aequus.sdk.publisher.Aequus;
import mobi.aequus.sdk.publisher.AequusAd;
import mobi.aequus.sdk.publisher.AequusAdError;
import mobi.aequus.sdk.publisher.AequusImpressionData;
import mobi.aequus.sdk.publisher.AequusIsAdReadyListener;
import mobi.aequus.sdk.publisher.AequusRewardedAd;
import mobi.aequus.sdk.publisher.RewardedInterstitialListener;

public class RewardedFragment extends FullPageAdFragment {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private static final String PLACEMENT_NAME = "YOUR_PLACEMENT_NAME";

    private AequusRewardedAd rewardedAd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        createRewardedAd();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void createRewardedAd() {
        rewardedAd = Aequus.createRewardedInterstitial(
                requireActivity(),
                PLACEMENT_NAME,
                createRewardedListener()
        );

        /**
         * Our SDK automatically caches ads in the background, so there is no need for the publisher to manually call `load`.
         * Just subscribe to this listener and it will always show the latest status of our internal ads cache.
         */
        rewardedAd.setIsAdReadyListener(new AequusIsAdReadyListener() {
            @Override
            public void onIsAdReadyStatusChanged(boolean isAdReady, @Nullable AequusImpressionData readyAdImpressionData) {
                String impressionDataJsonRepresentation =
                        readyAdImpressionData == null ? null : readyAdImpressionData.getJsonRepresentation();
                Log.i(TAG, "onIsAdReadyStatusChanged: isAdReady = " + isAdReady + ", readyAdImpressionData = " + impressionDataJsonRepresentation);
                showButton.setEnabled(isAdReady);
            }
        });

    }

    @Override
    protected void loadClick() {
        super.loadClick();
        rewardedAd.load();
    }

    @Override
    protected void showClick() {
        super.showClick();
        rewardedAd.show();
    }

    private RewardedInterstitialListener createRewardedListener() {
        return new RewardedInterstitialListener() {
            @Override
            public void onAdLoadSuccess(AequusAd aequusAd) {
                // Rewarded ad is ready to be shown. You should now show the ad by calling 'rewardedAd.show()'

                showToastAndLog("onAdLoadSuccess");
            }

            @Override
            public void onAdLoadFailed(AequusAdError aequusAdError) {
                // Rewarded ad failed to load. We recommend loading the next ad by calling 'rewardedAd.loadAd()' again.

                showToastAndLog("onAdLoadFailed() " + aequusAdError.getDescription());
            }

            @Override
            public void onAdShowSuccess(AequusAd aequusAd) {
                // Rewarded ad was successfully shown

                showToastAndLog("onAdShowSuccess");
            }

            @Override
            public void onAdShowFailed(AequusAdError aequusAdError) {
                // Rewarded ad failed to display. We recommend loading the next ad by calling 'rewardedAd.loadAd()'

                showToastAndLog("onAdShowFailed() " + aequusAdError.getDescription());
            }

            @Override
            public void onAdHidden(AequusAd aequusAd) {
                // Rewarded ad was closed

                showToastAndLog("onAdHidden");
            }

            @Override
            public void onAdClicked(AequusAd aequusAd) {
                // Rewarded ad was clicked

                showToastAndLog("onAdClicked");
            }

            @Override
            public void onRewardedVideoStarted(AequusAd aequusAd) {
                showToastAndLog("onRewardedVideoStarted");
            }

            @Override
            public void onRewardedVideoCompleted(AequusAd aequusAd) {
                showToastAndLog("onRewardedVideoCompleted");

            }

            @Override
            public void onUserRewarded(AequusAd aequusAd) {
                // User was rewarded. Here you should reward the user.

                showToastAndLog("onUserRewarded");
                showRewardDialog();
            }
        };
    }

    private void showRewardDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Reward")
                .setMessage("You got 1 coin")
                .create();
        alertDialog.show();
    }
}