package mobi.aequus.demo.mainscreen;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mobi.aequus.demo.R;

abstract class FullPageAdFragment extends Fragment {
    protected final String TAG = getClass().getSimpleName();

    Button loadButton;
    Button showButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(requireActivity(), R.layout.fragment_interstitial, container);
        loadButton = view.findViewById(R.id.load);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadClick();
            }
        });

        showButton = view.findViewById(R.id.show);
        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showClick();
            }
        });
        showButton.setEnabled(false);

        return view;
    }

    protected void loadClick() {
        showButton.setEnabled(false);
    }

    protected void showClick() {


    }

    protected void showToastAndLog(String msg) {
        Log.i(TAG, msg);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}