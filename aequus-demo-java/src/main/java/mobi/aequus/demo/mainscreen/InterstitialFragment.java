package mobi.aequus.demo.mainscreen;

import org.jetbrains.annotations.NotNull;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import mobi.aequus.sdk.publisher.Aequus;
import mobi.aequus.sdk.publisher.AequusAd;
import mobi.aequus.sdk.publisher.AequusAdError;
import mobi.aequus.sdk.publisher.AequusImpressionData;
import mobi.aequus.sdk.publisher.AequusInterstitialAd;
import mobi.aequus.sdk.publisher.AequusIsAdReadyListener;
import mobi.aequus.sdk.publisher.InterstitialListener;

public class InterstitialFragment extends FullPageAdFragment {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private static final String PLACEMENT_NAME = "YOUR_PLACEMENT_NAME";

    private AequusInterstitialAd interstitialAd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        createInterstitialAd();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void createInterstitialAd() {
        interstitialAd = Aequus.createInterstitial(
                requireActivity(),
                PLACEMENT_NAME,
                createInterstitialListener()
        );

        /**
         * Our SDK automatically caches ads in the background, so there is no need for the publisher to manually call `load`.
         * Just subscribe to this listener and it will always show the latest status of our internal ads cache.
         */
        interstitialAd.setIsAdReadyListener(new AequusIsAdReadyListener() {
            @Override
            public void onIsAdReadyStatusChanged(boolean isAdReady, @Nullable AequusImpressionData readyAdImpressionData) {
                String impressionDataJsonRepresentation =
                        readyAdImpressionData == null ? null : readyAdImpressionData.getJsonRepresentation();
                Log.i(TAG, "onIsAdReadyStatusChanged: isAdReady = " + isAdReady + ", readyAdImpressionData = " + impressionDataJsonRepresentation);
                showButton.setEnabled(isAdReady);
            }
        });
    }

    @Override
    protected void loadClick() {
        super.loadClick();
        interstitialAd.load();
    }

    @Override
    protected void showClick() {
        super.showClick();
        interstitialAd.show();
    }

    private InterstitialListener createInterstitialListener() {
        return new InterstitialListener() {

            @Override
            public void onAdClicked(@NotNull AequusAd aequusAd) {
                // Interstitial ad was clicked

                showToastAndLog("onAdClicked");
            }

            @Override
            public void onAdHidden(@NotNull AequusAd aequusAd) {
                // Interstitial ad was closed

                showToastAndLog("onAdHidden");

            }

            @Override
            public void onAdShowFailed(@NotNull AequusAdError aequusAdError) {
                // Interstitial ad failed to display. We recommend loading the next ad by calling 'interstitialAd.loadAd()'

                showToastAndLog("onAdShowFailed() " + aequusAdError.getDescription());
            }

            @Override
            public void onAdShowSuccess(@NotNull AequusAd aequusAd) {
                // Interstitial ad was successfully shown

                showToastAndLog("onAdShowSuccess");
            }

            @Override
            public void onAdLoadFailed(@NotNull AequusAdError aequusAdError) {
                // Interstitial ad failed to load. We recommend loading the next ad by calling 'interstitialAd.loadAd()' again.
                showToastAndLog("onAdLoadFailed() " + aequusAdError.getDescription());
            }

            @Override
            public void onAdLoadSuccess(@NotNull AequusAd aequusAd) {
                // Interstitial ad is ready to be shown. You should now show the ad by calling 'interstitialAd.show()'

                showToastAndLog("onAdLoadSuccess");

            }
        };
    }
}