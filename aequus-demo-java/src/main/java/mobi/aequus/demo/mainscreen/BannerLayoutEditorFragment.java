package mobi.aequus.demo.mainscreen;

import org.jetbrains.annotations.NotNull;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import mobi.aequus.demo.R;
import mobi.aequus.sdk.publisher.*;
import mobi.aequus.sdk.publisher.AequusAd;
import mobi.aequus.sdk.publisher.BannerListener;

public class BannerLayoutEditorFragment extends Fragment {
    private final String TAG = getClass().getSimpleName();

    private AequusBannerAdView bannerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(requireActivity(), R.layout.fragment_banner_layout_editor, container);
        bannerView = view.findViewById(R.id.aequus_banner);
        bannerView.setListener(createBannerListener());

        return view;
    }

    private BannerListener createBannerListener() {
        return new BannerListener() {
            @Override
            public void onAdLoadSuccess(@NotNull AequusAd aequusAd) {
                // Banner ad is was loaded. It will now be shown
                showToastAndLog("onAdLoadSuccess");
            }

            @Override
            public void onAdLoadFailed(@NotNull AequusAdError aequusAdError) {
                // Banner ad is was not loaded.
                showToastAndLog("onAdLoadFailed");
            }

            @Override
            public void onAdShowSuccess(@NotNull AequusAd aequusAd) {
                // Banner ad is was shown
                showToastAndLog("onAdShowSuccess");
            }

            @Override
            public void onAdShowFailed(@NotNull AequusAdError aequusAdError) {
                // Banner ad was not shown
                showToastAndLog("onAdShowFailed");
            }

            @Override
            public void onAdHidden(@NotNull AequusAd aequusAd) {
                // Banner ad was hidden
                showToastAndLog("onAdHidden");
            }

            @Override
            public void onAdClicked(@NotNull AequusAd aequusAd) {
                // Banner ad was clicked
                showToastAndLog("onAdClicked");
            }
        };
    }

    private void showToastAndLog(String msg) {
        Log.i(TAG, msg);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}