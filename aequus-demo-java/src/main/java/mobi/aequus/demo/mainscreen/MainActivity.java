package mobi.aequus.demo.mainscreen;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import mobi.aequus.demo.BuildConfig;
import mobi.aequus.demo.R;
import mobi.aequus.sdk.internal.AequusLogger;
import mobi.aequus.sdk.publisher.*;

public class MainActivity extends AppCompatActivity {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private static final String AEQUUS_API_KEY = "YOUR_API_KEY";
    /**
     * https://aequusads.com/docs/android/integration
     */
    private static final String AEQUUS_ACCESS_KEY = "YOUR_ACCESS_KEY";

    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_main);

        AequusPrivacy aequusPrivacy = AequusPrivacy.Builder()
                .CCPADoNotSell(true)
                .GDPRConsent(true)
                .underAgeOfConsent(true)
                .build();

        Log.d(TAG, "GDPRConsent ${aequusPrivacy.GDPRConsent}, GDPRUnderAgeOfConsent ${aequusPrivacy.GDPRUnderAgeOfConsent}, CCPADoNotSell ${aequusPrivacy.CCPADoNotSell}}");
        Aequus.setPrivacy(aequusPrivacy);
        Aequus.setUserId("user1337_demo");

        createPagerAdapter();

        AequusLogger.setLogEnabled(BuildConfig.DEBUG);
        Aequus.initialize(
                this,
                AEQUUS_API_KEY,
                AEQUUS_ACCESS_KEY,
                new AequusInitializationListener() {
                    @Override
                    public void onAequusInitializationStatus(boolean isInitialized, @NotNull String message) {
                        if (isInitialized) {
                            Toast.makeText(MainActivity.this, "Initialized", Toast.LENGTH_SHORT).show();

                            AequusILRDListener ilrdListener = new AequusILRDListener() {
                                @Override
                                public void onAequusILRDImpressionLoad(@NotNull AequusImpressionData impressionData) {
                                    Log.d("AEQUUS ILRD", "onLoad, json: " + impressionData.getJsonRepresentation());
                                    impressionData.getNetworkName();
                                }

                                /**
                                 Invoked when the ad was displayed successfully and the impression data was recorded
                                 **/
                                @Override
                                public void onAequusILRDImpressionShow(@NotNull AequusImpressionData impressionData) {
                                    Log.d("AEQUUS ILRD", "onShow, json: " + impressionData.getJsonRepresentation());

                                    // Feed impression data into internal tools or send to third-party analytics

/*
                                    FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(context);
                                    Bundle params = new Bundle();
                                    params.putString(FirebaseAnalytics.Param.AD_PLATFORM, "Aequus");
                                    params.putString(FirebaseAnalytics.Param.AD_SOURCE, impressionData.getNetworkName());
                                    params.putString(FirebaseAnalytics.Param.AD_FORMAT, impressionData.getAdFormatName());
                                    params.putString(FirebaseAnalytics.Param.AD_UNIT_NAME, impressionData.getNetworkAdUnitName());
                                    params.putDouble(FirebaseAnalytics.Param.VALUE, impressionData.getPublisherRevenue());
                                    params.putString(FirebaseAnalytics.Param.CURRENCY, impressionData.getCurrency());
                                    params.putString("precision", impressionData.getPrecision());
                                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.AD_IMPRESSION, params);
*/
                                }
                            };
                            Aequus.setILRDListener(ilrdListener);


                        } else {
                            AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Initialization error")
                                    .setMessage(message)
                                    .setPositiveButton(android.R.string.ok, null)
                                    .create();
                            dialog.show();

                            Log.d("QaAdEventLogger", message);
                        }
                    }
                });
    }

    private void createPagerAdapter() {
        final PagerAdapter pagerAdapter = new PagerAdapter(this);

        ViewPager2 viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(getString(pagerAdapter.pages.get(position).id));
            }
        }).attach();

    }

    private static class PagerAdapter extends FragmentStateAdapter {

        private final List<Page> pages = new ArrayList<>();

        public PagerAdapter(FragmentActivity fragmentActivity) {
            super(fragmentActivity);

            pages.add(new Page(new BannerProgrammaticFragment(), R.string.banner));
//            pages.add(new Page(new BannerLayoutEditorFragment(), R.string.banner));
            pages.add(new Page(new InterstitialFragment(), R.string.interstitial));
            pages.add(new Page(new RewardedFragment(), R.string.rewarded_video));
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return pages.get(position).fragment;
        }

        @Override
        public int getItemCount() {
            return pages.size();
        }

        private static class Page {
            Fragment fragment;
            int id;

            public Page(Fragment fragment, int id) {
                this.fragment = fragment;
                this.id = id;
            }
        }
    }
}

