package mobi.aequus.demo.mainscreen

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import mobi.aequus.demo.BuildConfig
import mobi.aequus.demo.R
import mobi.aequus.sdk.internal.AequusLogger
import mobi.aequus.sdk.publisher.*

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private val AEQUUS_API_KEY = "YOUR_API_KEY"

    /**
     * https://aequusads.com/docs/android/integration
     */
    private val AEQUUS_ACCESS_KEY = "YOUR_ACCESS_KEY"

    private val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.setTitle(R.string.title_activity_main);

        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {

                else -> false
            }
        }

        val aequusPrivacy = AequusPrivacy.Builder()
                .CCPADoNotSell(false)
                .GDPRConsent(true)
                .underAgeOfConsent(false)
                .build()

        Log.d(
                TAG,
                "GDPRConsent ${aequusPrivacy.GDPRConsent}, UnderAgeOfConsent ${aequusPrivacy.underAgeOfConsent}, CCPADoNotSell ${aequusPrivacy.CCPADoNotSell}}"
        );
        Aequus.setPrivacy(aequusPrivacy)
        Aequus.userId="user1337_demo"
        createPagerAdapter()

        AequusLogger.logEnabled = BuildConfig.DEBUG
        Aequus.initialize(
                this,
                AEQUUS_API_KEY,
                AEQUUS_ACCESS_KEY,
        ) { isInitialized, message ->
            if (isInitialized) {
                Toast.makeText(this, "Initialized", Toast.LENGTH_SHORT).show()
                val ilrdListener = object : AequusILRDListener {
                    override fun onAequusILRDImpressionLoad(impressionData: AequusImpressionData) {
                        // to access data use the 'impressionData' object'.
                        val networkName: String = impressionData.networkName

                        // get the the ‘impressionData’ object presented as a Json String
                        val jsonRepresentation = impressionData.getJsonRepresentation()
                    }

                    /**
                    Invoked when the ad was displayed successfully and the impression data was recorded
                     **/
                    @SuppressLint("MissingPermission")
                    override fun onAequusILRDImpressionShow(impressionData: AequusImpressionData) {
                        // Feed impression data into internal tools or send to third-party analytics

                        /*     val firebaseAnalytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(context)

                             val params = Bundle()
                             params.putString(FirebaseAnalytics.Param.AD_PLATFORM, "Aequus")
                             params.putString(FirebaseAnalytics.Param.AD_SOURCE, impressionData.networkName)
                             params.putString(FirebaseAnalytics.Param.AD_FORMAT, impressionData.adFormatName)
                             params.putString(FirebaseAnalytics.Param.AD_UNIT_NAME, impressionData.networkAdUnitName)
                             params.putDouble(FirebaseAnalytics.Param.VALUE, impressionData.publisherRevenue)
                             params.putString(FirebaseAnalytics.Param.CURRENCY, impressionData.currency)
                             params.putString("precision", impressionData.precision)

                             firebaseAnalytics.logEvent(FirebaseAnalytics.Event.AD_IMPRESSION, params)*/
                    }
                };

                Aequus.setILRDListener(ilrdListener);

            } else {
                val dialog = AlertDialog.Builder(this@MainActivity)
                        .setTitle("Initialization error")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, null)
                        .create()
                dialog.show()

                Log.d("QaAdEventLogger", message)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Aequus.setILRDListener(null)
    }

    private fun createPagerAdapter() {
        val pa = PagerAdapter(this@MainActivity)

        val vp = findViewById<ViewPager2>(R.id.pager)
        vp.adapter = pa

        val tl = findViewById<TabLayout>(R.id.tabs)
        TabLayoutMediator(tl, vp) { tab, position ->
            tab.text = getString(PagerAdapter[position].id)
        }.attach()
    }
}

private class PagerAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount() = pages.size

    override fun createFragment(position: Int) = pages[position].initializer()

    companion object {
        private val pages = arrayOf(
//                Page({ BannerLayoutEditorFragment() }, R.string.banner),
            Page({ BannerProgramaticFragment() }, R.string.banner),
            Page({ InterstitialFragment() }, R.string.interstitial),
            Page({ RewardedFragment() }, R.string.rewarded_video)
        )

        operator fun get(i: Int) = pages[i]
    }

    class Page(val initializer: () -> Fragment, val id: Int)
}
