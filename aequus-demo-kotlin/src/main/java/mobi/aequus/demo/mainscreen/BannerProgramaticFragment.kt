package mobi.aequus.demo.mainscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import mobi.aequus.demo.R
import mobi.aequus.sdk.publisher.*

class BannerProgramaticFragment : Fragment() {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private val PLACEMENT_NAME = "YOUR_PLACEMENT_NAME"

    private val TAG = javaClass.simpleName;
    private var bannerView: AequusBannerAdView? = null

    private lateinit var startButton: Button
    private lateinit var showButton: Button
    private lateinit var hideButton: Button
    private lateinit var bannerPlaceholder: FrameLayout
    private lateinit var progressBar: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = View
        .inflate(requireActivity(), R.layout.fragment_banner_pragrammatic, container)
        .also {
            startButton = it.findViewById(R.id.start)
            startButton.setOnClickListener { startClick() }

            showButton = it.findViewById(R.id.show)
            showButton.setOnClickListener { showClick() }
            showButton.isEnabled = false

            hideButton = it.findViewById(R.id.hide)
            hideButton.setOnClickListener { hideClick() }
            hideButton.isEnabled = false

            bannerPlaceholder = it.findViewById(R.id.banner_placeholder)
            progressBar = it.findViewById(R.id.banner_progressbar)
        }

    private fun createBannerAd() {
        bannerView = Aequus.createBanner(requireActivity(), PLACEMENT_NAME)?.apply {
            listener = createBannerListener()
        };
    }

    private fun startClick() {
        startButton.isEnabled = false
        showButton.isEnabled = true
        hideButton.isEnabled = true
        progressBar.visibility = View.VISIBLE

        createBannerAd()
        bannerPlaceholder.addView(
            bannerView, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
    }

    private fun hideClick() {
        bannerView?.hide()
    }

    private fun showClick() {
        bannerView?.show()
    }

    private fun createBannerListener() = object : BannerListener {
        override fun onAdLoadSuccess(aequusAd: AequusAd) {
            showToastAndLog("onAdLoadSuccess");
        }

        override fun onAdLoadFailed(aequusAdError: AequusAdError) {
            showToastAndLog("onAdLoadFailed");
        }

        override fun onAdShowSuccess(aequusAd: AequusAd) {
            showToastAndLog("onAdShowSuccess");
            progressBar.visibility=View.GONE
        }

        override fun onAdShowFailed(aequusAdError: AequusAdError) {
            showToastAndLog("onAdShowFailed");
        }

        override fun onAdHidden(aequusAd: AequusAd) {
            showToastAndLog("onAdHidden");
        }

        override fun onAdClicked(aequusAd: AequusAd) {
            showToastAndLog("onAdClicked");
        }
    }

    open fun showToastAndLog(msg : String) {
        Log.i(TAG, msg)
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }
}