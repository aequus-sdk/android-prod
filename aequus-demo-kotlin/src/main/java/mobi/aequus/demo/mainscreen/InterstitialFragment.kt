package mobi.aequus.demo.mainscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mobi.aequus.sdk.publisher.*

class InterstitialFragment : FullPageAdFragment() {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private val PLACEMENT_NAME = "YOUR_PLACEMENT_NAME"

    private var interstitialAd: AequusInterstitialAd? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        createInterstitialAd()

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun createInterstitialAd() {
        interstitialAd = Aequus.createInterstitial(
                requireActivity(),
                PLACEMENT_NAME,
                createInterstitialListener()
        )

        /**
         * Our SDK automatically caches ads in the background, so there is no need for the publisher to manually call `load`.
         * Just subscribe to this listener and it will always show the latest status of our internal ads cache.
         */
        interstitialAd?.setIsAdReadyListener((object : AequusIsAdReadyListener {
            override fun onIsAdReadyStatusChanged(
                isAdReady: Boolean,
                readyAdImpressionData: AequusImpressionData?,
            ) {
                Log.i(
                    TAG,
                    "onIsAdReadyStatusChanged: isAdReady = $isAdReady, readyAdImpressionData = ${readyAdImpressionData?.getJsonRepresentation()}"
                )
                showButton.isEnabled = isAdReady
            }
        }))
    }

    override fun loadClick() {
        super.loadClick();
        interstitialAd?.load();
    }

    override fun showClick() {
        super.showClick();
        interstitialAd?.show()
    }

    private fun createInterstitialListener() = object : InterstitialListener {
        override fun onAdLoadSuccess(aequusAd: AequusAd) {
            // Interstitial ad is ready to be shown. You should now show the ad by calling 'interstitialAd.show()'


            showToastAndLog("onAdLoadSuccess");
        }

        override fun onAdLoadFailed(aequusAdError: AequusAdError) {
            // Interstitial ad failed to load. We recommend loading the next ad by calling 'interstitialAd.loadAd()' again.

            showToastAndLog("onAdLoadFailed() ${aequusAdError.description}");
        }

        override fun onAdShowSuccess(aequusAd: AequusAd) {
            // Interstitial ad was successfully shown


            showToastAndLog("onAdShowSuccess");
        }

        override fun onAdShowFailed(aequusAdError: AequusAdError) {
            // Interstitial ad failed to display. We recommend loading the next ad by calling 'interstitialAd.loadAd()'


            showToastAndLog("onAdShowFailed() ${aequusAdError.description}");
        }

        override fun onAdHidden(aequusAd: AequusAd) {
            // Interstitial ad was closed

            showToastAndLog("onAdHidden");

        }

        override fun onAdClicked(aequusAd: AequusAd) {
            // Interstitial ad was clicked

            showToastAndLog("onAdClicked");
        }
    }
}