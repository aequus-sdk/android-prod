package mobi.aequus.demo.mainscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import mobi.aequus.demo.R

abstract class FullPageAdFragment : Fragment() {
    val TAG = javaClass.simpleName;

    private lateinit var loadButton: Button
    protected lateinit var showButton: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return View.inflate(requireActivity(), R.layout.fragment_interstitial, container)
            .also {
                loadButton = it.findViewById(R.id.load)
                loadButton.setOnClickListener { loadClick() }

                showButton = it.findViewById(R.id.show)
                showButton.setOnClickListener { showClick() }
                showButton.isEnabled = false
            }
    }

    open fun loadClick() {
        showButton.isEnabled = false
    }

    open fun showClick() {

    }
    open fun showToastAndLog(msg : String) {
        Log.i(TAG, msg)
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }
}