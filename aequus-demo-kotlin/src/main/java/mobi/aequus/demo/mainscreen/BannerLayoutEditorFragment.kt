package mobi.aequus.demo.mainscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import mobi.aequus.demo.R
import mobi.aequus.sdk.publisher.AequusAd
import mobi.aequus.sdk.publisher.AequusAdError
import mobi.aequus.sdk.publisher.AequusBannerAdView
import mobi.aequus.sdk.publisher.BannerListener

class BannerLayoutEditorFragment : Fragment() {
    val TAG = javaClass.simpleName;

    private var bannerView: AequusBannerAdView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
         View.inflate(requireActivity(), R.layout.fragment_banner_layout_editor, container)
        .apply {
            bannerView = findViewById(R.id.aequus_banner)
            bannerView?.listener = createBannerListener()
        }

    private fun createBannerListener() = object : BannerListener {
        override fun onAdLoadSuccess(aequusAd: AequusAd) {
            showToastAndLog("onAdLoadSuccess");
        }

        override fun onAdLoadFailed(aequusAdError: AequusAdError) {
            showToastAndLog("onAdLoadFailed");
        }

        override fun onAdShowSuccess(aequusAd: AequusAd) {
            showToastAndLog("onAdShowSuccess");
        }

        override fun onAdShowFailed(aequusAdError: AequusAdError) {
            showToastAndLog("onAdShowFailed");
        }

        override fun onAdHidden(aequusAd: AequusAd) {
            showToastAndLog("onAdHidden");
        }

        override fun onAdClicked(aequusAd: AequusAd) {
            showToastAndLog("onAdClicked");
        }
    }

    open fun showToastAndLog(msg : String) {
        Log.i(TAG, msg)
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

}