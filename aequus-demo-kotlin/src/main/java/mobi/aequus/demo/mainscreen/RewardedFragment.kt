package mobi.aequus.demo.mainscreen

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import mobi.aequus.sdk.publisher.*

class RewardedFragment : FullPageAdFragment() {
    /**
     * https://aequusads.com/docs/android/integration
     */
    private val PLACEMENT_NAME = "YOUR_PLACEMENT_NAME"

    private var rewardedAd: AequusRewardedAd? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        createRewardedAd()

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun createRewardedAd() {
        rewardedAd = Aequus.createRewardedInterstitial(
                requireActivity(),
                PLACEMENT_NAME,
                createRewardedListener()
        )

        /**
         * Our SDK automatically caches ads in the background, so there is no need for the publisher to manually call `load`.
         * Just subscribe to this listener and it will always show the latest status of our internal ads cache.
         */
        rewardedAd?.setIsAdReadyListener((object : AequusIsAdReadyListener {
            override fun onIsAdReadyStatusChanged(
                isAdReady: Boolean,
                readyAdImpressionData: AequusImpressionData?,
            ) {
                Log.i(
                    TAG,
                    "onIsAdReadyStatusChanged: isAdReady = $isAdReady, readyAdImpressionData = ${readyAdImpressionData?.getJsonRepresentation()}"
                )
                showButton.isEnabled = isAdReady
            }
        }))
    }

    override fun loadClick() {
        super.loadClick();
        rewardedAd?.load();
    }

    override fun showClick() {
        super.showClick();
        rewardedAd?.show()
    }

    private fun createRewardedListener() = object : RewardedInterstitialListener {
        override fun onAdLoadSuccess(aequusAd: AequusAd) {
            // Rewarded ad is ready to be shown. You should now show the ad by calling 'rewardedAd.show()'

            showToastAndLog("onAdLoadSuccess");
        }

        override fun onAdLoadFailed(aequusAdError: AequusAdError) {
            // Rewarded ad failed to load. We recommend loading the next ad by calling 'rewardedAd.loadAd()' again.

            showToastAndLog("onAdLoadFailed() ${aequusAdError.description}");
        }

        override fun onAdShowSuccess(aequusAd: AequusAd) {
            // Rewarded ad was successfully shown

            showToastAndLog("onAdShowSuccess");
        }

        override fun onAdShowFailed(aequusAdError: AequusAdError) {
            // Rewarded ad failed to display. We recommend loading the next ad by calling 'rewardedAd.loadAd()'

            showToastAndLog("onAdShowFailed() ${aequusAdError.description}");
        }

        override fun onAdHidden(aequusAd: AequusAd) {
            // Rewarded ad was closed

            showToastAndLog("onAdHidden");
        }

        override fun onAdClicked(aequusAd: AequusAd) {
            // Rewarded ad was clicked

            showToastAndLog("onAdClicked");
        }

        override fun onRewardedVideoStarted(aequusAd: AequusAd) {
            showToastAndLog("onRewardedVideoStarted")
        }

        override fun onRewardedVideoCompleted(aequusAd: AequusAd) {
            showToastAndLog("onRewardedVideoCompleted")

        }

        override fun onUserRewarded(aequusAd: AequusAd) {
            // User was rewarded. Here you should reward the user.

            showToastAndLog("onUserRewarded")
            showRewardDialog();
        }
    }

    private fun showRewardDialog() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle("Reward")
                setMessage("You got 1 coin")
            }
            // Set other dialog properties

            // Create the AlertDialog
            builder.create()
        }
        alertDialog?.show();
    }
}