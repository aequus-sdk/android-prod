# Aequus SDK


## Demo Apps
To get started with the demo apps, follow the instructions below:

1. Open your desired project in Android Studio: `aequus-demo-kotlin` or `aequus-demo-java`.
2. In the MainActivity class set the 'API_KEY' and 'ACCESS_KEY' constants. Documentation for obtaining those values is here:
   https://developers.aequus.mobi/
3. Launch the project and you should be getting ads.
